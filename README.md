# Blog Post Template With 2 Columns and Featured Image

This requires adding some simple CSS to the existing theme. As well you must create an article template and article section. 


[View Example Page\Post Here](https://blogpost-buybutton-demo.myshopify.com/blogs/news/example-post-with-featured-image)

1 . Add the code in [theme.scss.liquid](https://bitbucket.org/mttcmrn/blog-post-template-with-2-columns-and-featured-image/src/8fb741a360119e151069ecdf03848294507b740e/theme.scss.liquid?at=master) to the existing scss file in the store theme code. Different themes may have different scss names. The default is theme.scss.liquid 

2 . Create a new article template using the code shown in [article.with_featured_image.liquid](https://bitbucket.org/mttcmrn/blog-post-template-with-2-columns-and-featured-image/src/8fb741a360119e151069ecdf03848294507b740e/article.with_featured_image.liquid?at=master)

![alt](https://screenshot.click/4229-27-11-xax74.jpg)

3 . Create a new section named exactly `article-template_for_featured_template.liquid` using the code shown in [article-template_for_featured_template.liquid](https://bitbucket.org/mttcmrn/blog-post-template-with-2-columns-and-featured-image/src/8fb741a360119e151069ecdf03848294507b740e/article-template_for_featured_template.liquid?at=master)

4 . Change the template for the blog post to the one you just created. 

![alt](https://screenshot.click/4229-27-11-p47tc.jpg)


![alt](https://screenshot.click/4229-27-11-7pxhe.jpg)